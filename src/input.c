/* ************************************************************************** */
/*                                                                            */
/*                                                             ::::::::       */
/*   input.c                                                 :+:    :+:       */
/*                                                          +:+               */
/*   By: nekonoor <nekonoor@protonmail.com>                +#+                */
/*                                                        +#+                 */
/*   Created: 2019/09/02 22:56:32 by nekonoor            #+#    #+#           */
/*   Updated: 2019/09/04 18:06:10 by nekonoor            ########   odam.nl   */
/*                                                                            */
/* ************************************************************************** */

#include "frainbuck.h"
#include "libuseful.h"
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>

char	*buf_cat(char *out, char *buf)
{
	char	*ret;

	ret = (char*)malloc(sizeof(char) * (uf_strlen(out) + uf_strlen(buf) + 1));
	if (ret == NULL)
		return (NULL);
	uf_strcpy(ret, out);
	uf_strcpy(ret + uf_strlen(out), buf);
	free(out);
	return (ret);
}

char	*read_file(char *path)
{
	char	*buf;
	char	*out;
	int		size;
	int		fd;

	buf = (char*)malloc(sizeof(char) * (BUF_SIZE + 1));
	if (buf == NULL)
		return (NULL);
	if (path != NULL)
	{
		fd = open(path, O_RDONLY);
		if (errno != 0)
			return (NULL);
	}
	else
		fd = 0;
	out = (char*)malloc(sizeof(char));
	if (out == NULL)
		return (NULL);
	size = 1;
	while (size)
	{
		size = read(fd, buf, BUF_SIZE);
		if (errno != 0)
			return (NULL);
		buf[size] = '\0';
		out = buf_cat(out, buf);
		if (out == NULL)
			return (NULL);
	}
	if (fd != 0)
	{
		close(fd);
		if (errno != 0)
			return (NULL);
	}
	free(buf);
	return (out);
}
