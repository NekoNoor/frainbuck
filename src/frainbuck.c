/* ************************************************************************** */
/*                                                                            */
/*                                                             ::::::::       */
/*   frainbuck.c                                             :+:    :+:       */
/*                                                          +:+               */
/*   By: nekonoor <nekonoor@protonmail.com>                +#+                */
/*                                                        +#+                 */
/*   Created: 2019/09/02 20:44:31 by nekonoor            #+#    #+#           */
/*   Updated: 2019/09/04 18:06:16 by nekonoor            ########   odam.nl   */
/*                                                                            */
/* ************************************************************************** */

#include "frainbuck.h"
#include "libuseful.h"
#include <stdlib.h>

char	*matchbrace(char *str)
{
	int	c;

	c = 1;
	if (*str == '[')
		while (c)
		{
			str++;
			if (*str == '[')
				c++;
			if (*str == ']')
				c--;
		}
	else
		while (c)
		{
			str--;
			if (*str == '[')
				c--;
			if (*str == ']')
				c++;
		}
	return (str);
}

void	frainbuck(char *str)
{
	char	fb[2048];
	int		i;

	uf_bzero(fb, sizeof(fb));
	i = 0;
	while (*str)
	{
		if (*str == '>')
			i++;
		if (*str == 'r')
			i += uf_atoc(++str);
		if (*str == '<')
			i--;
		if (*str == 'l')
			i -= uf_atoc(++str);
		if (*str == '+')
			fb[i]++;
		if (*str == 'a')
			fb[i] += uf_atoc(++str);
		if (*str == '-')
			fb[i]--;
		if (*str == 's')
			fb[i] -= uf_atoc(++str);
		if (*str == '.')
			uf_putchar(fb[i]);
		if (*str == ',')
			fb[i] = uf_atoc(++str);
		if (*str == '[' && fb[i] == 0)
			str = matchbrace(str);
		if (*str == ']' && fb[i] != 0)
			str = matchbrace(str);
		str++;
	}
}

char	*fb_inter(char *fb, int count, char *neg, char *pos)
{
	if (count > 0)
	{
		fb = uf_strcat(fb, pos);
		fb = uf_strcat(fb, uf_ctoa(count));
	}
	if (count < 0)
	{
		fb = uf_strcat(fb, neg);
		fb = uf_strcat(fb, uf_ctoa(-count));
	}
	return (fb);
}

char	*bf_to_fb(char *bf)
{
	char	*orig;
	char	*fb;
	int		add;
	int		jmp;

	fb = (char*)malloc(sizeof(char) * uf_strlen(bf));
	if (fb == NULL)
		return (NULL);
	orig = fb;
	uf_bzero(fb, uf_strlen(bf));
	while (*bf)
	{
		add = 0;
		while (*bf == '+' || *bf == '-')
		{
			(*bf == '+') ? add++ : add--;
			bf++;
		}
		fb = fb_inter(fb, add, "s", "a");
		fb += uf_strlen(fb);
		jmp = 0;
		while (*bf == '>' || *bf == '<')
		{
			(*bf == '>') ? jmp++ : jmp--;
			bf++;
		}
		fb = fb_inter(fb, jmp, "l", "r");
		fb += uf_strlen(fb);
		while (*bf != '+' && *bf != '-' && *bf != '>' && *bf != '<' && *bf)
			*fb++ = *bf++;
	}
	*fb = '\0';
	return (orig);
}

int		main(int ac, char **av)
{
	char	*str;

	if (ac <= 2)
	{
		str = read_file(av[1]);
		if (str == NULL)
			return (1);
		str = bf_to_fb(str);
		frainbuck(str);
	}
	if (ac > 2)
	{
		uf_putstr("Usage: ");
		uf_putstr(av[0]);
		uf_putstr(" <file>\n\nIf no file is given, program reads from stdin\n");
	}
	return (0);
}
