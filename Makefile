# **************************************************************************** #
#                                                                              #
#                                                              ::::::::        #
#    Makefile                                                :+:    :+:        #
#                                                           +:+                #
#    By: nekonoor <nekonoor@protonmail.com>                +#+                 #
#                                                         +#+                  #
#    Created: 2019/09/02 20:51:55 by nekonoor            #+#    #+#            #
#    Updated: 2019/09/04 18:24:41 by nekonoor            ########   odam.nl    #
#                                                                              #
# **************************************************************************** #

CC = clang-8
CFLAGS = -g -Wall -Wextra -Werror

INC = -I./include/ -I./libuseful/include/
SRC = frainbuck.c input.c

ODIR = ./obj/
OBJ = $(addprefix $(ODIR),$(SRC:.c=.o))

LIB = ./libuseful/libuseful.a

NAME = frainbuck

vpath %.c ./src

.PHONY: clean fclean

all: $(NAME)

$(NAME): $(OBJ) $(LIB)
	$(CC) $(CFLAGS) $(INC) -o $(NAME) $(OBJ) $(LIB)

$(LIB):
	$(MAKE) -C $(dir $(LIB))

$(ODIR):
	mkdir $@

$(ODIR)%.o: %.c $(ODIR)
	$(CC) $(CFLAGS) $(INC) -c $< -o $@

clean:
	$(RM) -r $(ODIR)
	$(MAKE) -C $(dir $(LIB)) clean

fclean: clean
	$(RM) $(NAME)
	$(MAKE) -C $(dir $(LIB)) fclean

re: fclean all
