/* ************************************************************************** */
/*                                                                            */
/*                                                             ::::::::       */
/*   frainbuck.h                                             :+:    :+:       */
/*                                                          +:+               */
/*   By: nekonoor <nekonoor@protonmail.com>                +#+                */
/*                                                        +#+                 */
/*   Created: 2019/09/02 20:44:39 by nekonoor            #+#    #+#           */
/*   Updated: 2019/09/04 17:59:06 by nekonoor            ########   odam.nl   */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRAINBUCK_H
# define FRAINBUCK_H
# define BUF_SIZE 4096

char	*matchbrace(char *str);
void	frainbuck(char *str);
char	*fb_inter(char *fb, int count, char *neg, char *pos);
char	*bf_to_fb(char *bf);
char	*buf_cat(char *dst, char *buf);
char	*read_file(char *path);

#endif
